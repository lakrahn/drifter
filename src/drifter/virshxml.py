from __future__ import annotations
import copy
import io

from lxml.etree import ElementTree, XML, _ElementTree as Element  # nosec


class VirshConfig(object):
    def __init__(
        self,
        data: Element,
    ) -> None:
        self.data = data

    @classmethod
    def from_xml(cls, data: str) -> VirshConfig:
        root = ElementTree(XML(data))
        return VirshConfig(data=root)

    def to_xml(self) -> str:
        output = io.BytesIO()
        self.data.write(
            output,
            method="xml",
            encoding="utf-8",
            pretty_print=True
        )
        return output.getvalue().decode()

    def disks(self) -> list[str]:
        sources = self.data.findall(
            'devices/disk[@device="disk"]/source'
        )
        disks = []
        for disk in sources:
            disks.append(disk.attrib["file"])
        return disks

    def remove_disk(self, path: str) -> VirshConfig:
        data = copy.deepcopy(self.data)
        disks = data.xpath(
            'devices/disk[@device="disk" and source[@file=$path]]',
            path=path
        )
        for disk in disks:
            disk.getparent().remove(disk)
        return VirshConfig(data)
