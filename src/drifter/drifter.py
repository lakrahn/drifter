#!/usr/bin/env python3
from __future__ import annotations
import argparse
import copy
import dataclasses
import io
import json
import logging
import os
import pathlib
import shutil
import socket
import string
import subprocess  # nosec
import sys
import tempfile
import textwrap
import traceback

from drifter.virshxml import VirshConfig
from contextlib import contextmanager
from enum import Enum

from typing import (
    Dict,
    Generator,
    List,
    Optional,
    Tuple,
)

import bcrypt
import requests
import yaml


_log = logging.getLogger(__name__)


class UnknownVariant(Exception):
    pass


class StartupFailed(Exception):
    pass


class Variant(Enum):
    centos8 = "centos8"

    @classmethod
    def from_str(cls, name: str) -> 'Variant':
        for v in cls:
            if name == v.value:
                return v
        raise UnknownVariant(f"Variant '{name}' not known to drifter.")


VARIANT_SOURCE = {
    Variant.centos8: {
        "base_url": "https://cloud.centos.org/centos/8/x86_64/images/",
        "checksum": "CHECKSUM",
        "signature": "CHECKSUM.asc",
        "image": "CentOS-8-GenericCloud-8.3.2011-20201204.2.x86_64.qcow2",
        "public_key": """
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.22 (GNU/Linux)

mQINBFzMWxkBEADHrskpBgN9OphmhRkc7P/YrsAGSvvl7kfu+e9KAaU6f5MeAVyn
rIoM43syyGkgFyWgjZM8/rur7EMPY2yt+2q/1ZfLVCRn9856JqTIq0XRpDUe4nKQ
8BlA7wDVZoSDxUZkSuTIyExbDf0cpw89Tcf62Mxmi8jh74vRlPy1PgjWL5494b3X
5fxDidH4bqPZyxTBqPrUFuo+EfUVEqiGF94Ppq6ZUvrBGOVo1V1+Ifm9CGEK597c
aevcGc1RFlgxIgN84UpuDjPR9/zSndwJ7XsXYvZ6HXcKGagRKsfYDWGPkA5cOL/e
f+yObOnC43yPUvpggQ4KaNJ6+SMTZOKikM8yciyBwLqwrjo8FlJgkv8Vfag/2UR7
JINbyqHHoLUhQ2m6HXSwK4YjtwidF9EUkaBZWrrskYR3IRZLXlWqeOi/+ezYOW0m
vufrkcvsh+TKlVVnuwmEPjJ8mwUSpsLdfPJo1DHsd8FS03SCKPaXFdD7ePfEjiYk
nHpQaKE01aWVSLUiygn7F7rYemGqV9Vt7tBw5pz0vqSC72a5E3zFzIIuHx6aANry
Gat3aqU3qtBXOrA/dPkX9cWE+UR5wo/A2UdKJZLlGhM2WRJ3ltmGT48V9CeS6N9Y
m4CKdzvg7EWjlTlFrd/8WJ2KoqOE9leDPeXRPncubJfJ6LLIHyG09h9kKQARAQAB
tDpDZW50T1MgKENlbnRPUyBPZmZpY2lhbCBTaWduaW5nIEtleSkgPHNlY3VyaXR5
QGNlbnRvcy5vcmc+iQI3BBMBAgAhBQJczFsZAhsDBgsJCAcDAgYVCAIJCgsDFgIB
Ah4BAheAAAoJEAW1VbOEg8ZdjOsP/2ygSxH9jqffOU9SKyJDlraL2gIutqZ3B8pl
Gy/Qnb9QD1EJVb4ZxOEhcY2W9VJfIpnf3yBuAto7zvKe/G1nxH4Bt6WTJQCkUjcs
N3qPWsx1VslsAEz7bXGiHym6Ay4xF28bQ9XYIokIQXd0T2rD3/lNGxNtORZ2bKjD
vOzYzvh2idUIY1DgGWJ11gtHFIA9CvHcW+SMPEhkcKZJAO51ayFBqTSSpiorVwTq
a0cB+cgmCQOI4/MY+kIvzoexfG7xhkUqe0wxmph9RQQxlTbNQDCdaxSgwbF2T+gw
byaDvkS4xtR6Soj7BKjKAmcnf5fn4C5Or0KLUqMzBtDMbfQQihn62iZJN6ZZ/4dg
q4HTqyVpyuzMXsFpJ9L/FqH2DJ4exGGpBv00ba/Zauy7GsqOc5PnNBsYaHCply0X
407DRx51t9YwYI/ttValuehq9+gRJpOTTKp6AjZn/a5Yt3h6jDgpNfM/EyLFIY9z
V6CXqQQ/8JRvaik/JsGCf+eeLZOw4koIjZGEAg04iuyNTjhx0e/QHEVcYAqNLhXG
rCTTbCn3NSUO9qxEXC+K/1m1kaXoCGA0UWlVGZ1JSifbbMx0yxq/brpEZPUYm+32
o8XfbocBWljFUJ+6aljTvZ3LQLKTSPW7TFO+GXycAOmCGhlXh2tlc6iTc41PACqy
yy+mHmSv
=kkH7
-----END PGP PUBLIC KEY BLOCK-----
        """.strip(),
    }
}


class Defaults(object):
    cpus = 1
    memory = 1024
    disks = [10, ]
    variant = Variant.centos8
    cache_dir = str(os.path.expanduser("~/.cache/drifter/"))
    password = bcrypt.hashpw("drifter".encode(), bcrypt.gensalt()).decode('utf8')


def deep_merge(a: Dict, b: Dict) -> Dict:
    """Fold two dictionary trees into another.

    The original dicts are unaffected.

        >>> a = {
        ...   "key": 23,
        ...   "obj": {"int": 1, "list": [1,2,3], "static": "world"},
        ... }
        >>> b = {
        ...   "key": 42,
        ...   "obj": {"int": 2, "list": [1,1,2,3,5] },
        ... }
        >>> deep_merge(a, b)
        {'key': 42, 'obj': {'int': 2, 'list': [1, 1, 2, 3, 5], 'static': 'world'}}

    """
    res = copy.deepcopy(a)

    for key, value in b.items():
        if isinstance(value, Dict) and key in a:
            res[key] = deep_merge(a[key], value)
        else:
            res[key] = value
    return res


class AnsibleConfig(object):

    def __init__(self, inventory: Dict, location: str) -> None:
        self.inventory = inventory
        self.location = location

    @classmethod
    def from_file(cls, file: str) -> 'AnsibleConfig':
        proc = subprocess.run(  # nosec
            ["ansible-inventory", "-i", file, "--list"],
            text=True,
            capture_output=True,
            check=True
        )
        data = json.loads(proc.stdout)
        inventory_basedir = str(pathlib.Path(file).parent.resolve())
        return cls.from_json(data, location=inventory_basedir)

    @classmethod
    def remap_host_groups(
        cls,
        data: Dict,
        current_group='all',
        group_stack=None,
        output=None
    ) -> Dict:
        """Remap ansibles group -> host mapping to host -> groups."""
        if output is None:
            output = {}
        if group_stack is None:
            group_stack = [current_group, ]

        for host in data.get(current_group, {}).get("hosts", []):
            output[host] = copy.copy(group_stack)

        for group in data.get(current_group, {}).get("children", []):
            local_group_stack = copy.copy(group_stack)
            local_group_stack.append(group)
            cls.remap_host_groups(
                data,
                current_group=group,
                group_stack=local_group_stack,
                output=output
            )

        return output

    @classmethod
    def from_json(
        cls,
        data: Dict,
        location: Optional[str] = None
    ) -> 'AnsibleConfig':
        if location is None:
            location = os.getcwd()

        host_groups = cls.remap_host_groups(data)
        inventory = {}

        for (host, groups) in host_groups.items():
            inventory[host] = {
                'groups': groups,
                'variables': data['_meta']['hostvars'].get(host, {}),
            }
        return cls(inventory, location)

    def machines(self) -> List['Machine']:
        return list(map(
            lambda name_cfg: Machine(name_cfg[0], name_cfg[1], self.location),
            self.inventory.items(),
        ))


@dataclasses.dataclass
class DiskManagement():
    attach_disks: list[Disk]
    remove_disks: list[str]
    grow_disks: list[Disk]
    machine: Machine

    @classmethod
    def from_machine(
        cls,
        machine: Machine
    ) -> DiskManagement:
        config_disks = machine.disks()
        virsh_disks = machine.current_disks()
        attach_disks = list()
        remove_disks = list()
        grow_disks = list()

        for cdisk in config_disks:
            if cdisk.file in virsh_disks:
                grow_disks.append(cdisk)
            else:
                attach_disks.append(cdisk)

        configured_disk_names = [d.file for d in config_disks]
        for vdisk in virsh_disks:
            if vdisk not in configured_disk_names:
                remove_disks.append(vdisk)
        return DiskManagement(
            attach_disks=attach_disks,
            remove_disks=remove_disks,
            grow_disks=grow_disks,
            machine=machine
        )

    def attach(self) -> None:
        for disk in self.attach_disks:
            self.machine.add_disk(disk)
            _log.info(f"Attached disk {disk.file} of {self.machine.uid()}")

    def remove(self) -> None:
        for disk in self.remove_disks:
            self.machine.remove_disk(disk)
            _log.info(f"Removed disk {disk} of {self.machine.uid()}")

    def grow(self) -> None:
        for disk in self.grow_disks:
            disk.grow()
            _log.info(f"Grew disk {disk.file} of {self.machine.uid()}")


@dataclasses.dataclass
class Disk(object):
    size: int
    index: int
    file: str

    def install_arg(self) -> List[str]:
        return [
            "--disk",
            f"{self.file},size={self.size},format=qcow2,bus=virtio",
        ]

    def grow(self) -> None:
        try:
            subprocess.run(  # nosec
                ["qemu-img", "resize", self.file, f"{self.size}G"],
                capture_output=True,
                text=True,
                check=True,
            )
        except subprocess.CalledProcessError as e:
            error = textwrap.indent(e.stderr, "  ")
            raise StartupFailed(f"Failed to grow disk:\n{error}") from None

    def from_template(self, disk: str) -> None:
        try:
            os.makedirs(os.path.dirname(self.file))
        except FileExistsError:
            pass
        _log.info(f"Using '{disk}' as disk template.")
        shutil.copy(disk, self.file)

    def virsh_target(self) -> str:
        """ Returns the virsh target based upon its own index within the disks.

        >>> Disk(file="", size=0, index=3).virsh_target()
        'vdd'

        """
        if self.index > 25:
            msg = 'Achievement unlocked: "Logic for more disks needed"'
            raise ValueError(msg)
        return 'vd' + string.ascii_lowercase[self.index]


class Machine(object):
    def __init__(self, name: str, config: Dict, location: str) -> None:
        self.name = name
        self._groups = config['groups']
        self._vars = config['variables']
        self.location = location

    def uid(self) -> str:
        return self.name

    def variant(self) -> str:
        return self._vars.get("drifter_variant", Defaults.variant.value)

    def _resolve(self, value: str) -> str:
        try:
            addr = socket.gethostbyname(value)
        except socket.gaierror:
            raise ValueError(f'Failed to resolve hostname "{value}".')
        return addr

    def address(self) -> Optional[str]:
        try:
            return self._resolve(self._vars.get("ansible_host", self.name))
        except ValueError:
            return None

    def cpus(self) -> int:
        return self._vars.get("drifter_cpus", Defaults.cpus)

    def memory(self) -> int:
        """Memory in MiB."""
        return self._vars.get("drifter_memory", Defaults.memory)

    def disks(self) -> List[Disk]:
        param = self._vars.get("drifter_disk", Defaults.disks)
        if isinstance(param, int):
            param = [param, ]
        disks = []
        for i, d in enumerate(param):
            size = int(d)
            file = os.path.join(
                self.location,
                ".drifter",
                f"{self.uid()}-{i}.qcow2"
            )
            disks.append(Disk(size=size, file=file, index=i))

        return disks

    def current_disks(self) -> List[str]:
        """Get a list of currently attached disks."""
        dumped_virsh = subprocess.check_output(  # nosec
            ["virsh", "dumpxml", self.uid()],
            text=True
        )
        virsh_config = VirshConfig.from_xml(dumped_virsh)
        return virsh_config.disks()

    def user_data(self) -> Optional[str]:
        return self._vars.get("drifter_user_data", None)

    def cloud_init_args(self) -> Tuple[str, List[str]]:
        args = ["root-password-generate=off", ]

        ud = self.user_data()

        if ud is None:
            user_data = UserData({})
        else:
            user_data = UserData.read(ud)

        ud_path = tempfile.NamedTemporaryFile(
            prefix="drifter_", delete=False
        ).name

        user_data.write(ud_path)
        args.append(f"user-data={ud_path}")

        ed25519_pub = os.path.expanduser("~/.ssh/id_ed25519.pub")
        if os.path.exists(ed25519_pub):
            args.append(f"ssh-key={ed25519_pub}")

        return ",".join(args), [ud_path, ]

    def is_configured(self) -> bool:
        proc = subprocess.run(  # nosec
            ["virsh", "list", "--all", "--name"],
            text=True,
            capture_output=True,
            check=True
        )
        return self.uid() in proc.stdout.splitlines()

    @contextmanager
    def _virt_install_cmd(
        self,
        disks: List[Disk],
    ) -> Generator[List[str], None, None]:
        disk_args = []

        for d in disks:
            disk_args += d.install_arg()

        args, files = self.cloud_init_args()

        install_cmd = [
            "virt-install",
            "--connect=qemu:///session",
            "--name", f"{self.uid()}",
            "--memory", f"{self.memory()}",
            "--cpu", "host",
            "--vcpus", f"{self.cpus()}",
            "--graphics", "none",
            "--os-type", "linux",
            "--os-variant", self.variant(),
            "--wait", "-1",
            "--noreboot",
            "--import",
            *disk_args,
            "--network", "bridge=virbr0,model=virtio",
            "--cloud-init", args,
        ]
        try:
            yield install_cmd
        finally:
            for file in files:
                os.unlink(file)

    def remove_disk(self, disk_file: str):
        virsh_config_xml = subprocess.check_output(  # nosec
            ["virsh", "dumpxml", self.uid()],
            text=True
        )
        virsh_config = VirshConfig.from_xml(virsh_config_xml)
        virsh_config = virsh_config.remove_disk(disk_file)
        with tempfile.NamedTemporaryFile(prefix='drifter-') as tf:
            tf.write(virsh_config.to_xml().encode())
            tf.flush()
            try:
                subprocess.run(  # nosec
                    ["virsh", "define", tf.name],
                    check=True
                )
            except subprocess.CalledProcessError:
                _log.critical("Could not delete the disk from the VM.")
                raise

        os.unlink(disk_file)

    def add_disk(self, disk: Disk):
        if not pathlib.Path(disk.file).exists():
            subprocess.run(  # nosec
                ["qemu-img", "create", disk.file, f"{disk.size}G"],
                check=True
            )
        target = disk.virsh_target()
        subprocess.run(  # nosec
            ["virsh", "attach-disk", self.uid(), disk.file, target, "--config"],
            check=True
        )

    def start(self) -> None:
        proc = subprocess.run(  # nosec
            ["virsh", "list", "--state-running", "--name"],
            text=True,
            capture_output=True,
            check=True
        )
        plan = DiskManagement.from_machine(self)
        if not self.uid() in proc.stdout.splitlines():
            plan.grow()
            plan.remove()
            plan.attach()
            _log.info(f"Starting machine {self.uid()}")
            subprocess.run(  # nosec
                ["virsh", "start", self.uid()],
                capture_output=True,
                check=True
            )

    def halt(self) -> None:
        proc = subprocess.run(  # nosec
            ["virsh", "list", "--state-running", "--name"],
            text=True,
            capture_output=True,
            check=True
        )
        if self.uid() in proc.stdout.splitlines():
            _log.info(f"Sending graceful shutdown to {self.uid()}")
            try:
                subprocess.run(  # nosec
                    ["virsh", "shutdown", self.uid()],
                    capture_output=True,
                    timeout=60,
                    check=True
                )
            except subprocess.TimeoutExpired:
                _log.warning("Timeout for graceful shutdown expired.")
                _log.warning(f"Forcefully shutting down {self.uid()}")
                subprocess.run(  # nosec
                    ["virsh", "destroy", self.uid()],
                    capture_output=True,
                    check=True
                )
        else:
            _log.info(f"Machine {self.uid()} already not running")

    def destroy(self) -> None:
        self.halt()
        proc = subprocess.run(  # nosec
            ["virsh", "list", "--all", "--name"],
            text=True,
            capture_output=True,
            check=True
        )
        if self.uid() in proc.stdout.splitlines():
            _log.info(f"Utter destruction of {self.uid()}")
            all_disks = self.current_disks()
            subprocess.run(  # nosec
                ["virsh", "undefine", self.uid()],
                capture_output=True,
                check=True
            )
            for disk in all_disks:
                try:
                    os.unlink(disk)
                    _log.info(f"Disk file {disk} removed.")
                except FileNotFoundError:
                    _log.info(f"Disk file {disk} not found.")

    def attach(self) -> None:
        proc = subprocess.run(  # nosec
            ["virsh", "list", "--state-running", "--name"],
            text=True,
            capture_output=True,
            check=True
        )
        if self.uid() in proc.stdout.splitlines():
            cmd = ['virsh', '--escape', '^g', 'console', self.uid()]
            _log.debug(f'Attach command:  {" ".join(cmd)}')
            subprocess.run(  # nosec
                cmd,
                stdin=sys.stdin,
                check=True,
                shell=False
            )
        else:
            _log.info(f'Could not attach to "{self.uid()}".')
            _log.info('Machine not found.')

    def cloud_init(self) -> None:
        if self.is_configured():
            _log.info(f"Machine {self.name} already available")
            self.start()
            return

        base_image = os.path.join(
            Defaults.cache_dir,
            'images',
            f'{self.variant()}.qcow2'
        )

        sda = self.disks()[0]
        sda.from_template(base_image)

        _log.info(f'Defining machine "{self.name}" as "{self.uid()}"')
        with self._virt_install_cmd(self.disks()) as install_cmd:
            _log.debug(f'Running "{" ".join(install_cmd)}"')
            subprocess.run(install_cmd, check=True)  # nosec

        self.start()


class UserData(object):

    def __init__(self, data: Dict) -> None:
        self._data = data

    @classmethod
    def read(cls, file: str) -> 'UserData':
        if file.endswith('.json'):
            with open(file, 'r') as fp:
                data = json.load(fp)
        if file.endswith('.yaml'):
            with open(file, '') as fp:
                data = yaml.safe_load(fp)
        return cls(data)

    def write(self, file: str, inject_user=True) -> None:
        data = copy.deepcopy(self._data)

        if inject_user:
            data.setdefault('users', [])

            data['users'].append({
                "name": "drifter",
                "sudo": "ALL=(ALL) NOPASSWD:ALL",
                "passwd": Defaults.password,
                "lock_passwd": False,
            })
        data["power_state"] = {
            "mode": "poweroff",
            "message": "Hurray time for shutdown after installation",
            "condition": True
        }

        cfg = io.StringIO()

        with open(file, "w") as fp:
            fp.write("#cloud-config\n")
            cfg.write("#cloud-config\n")
            yaml.dump(data, fp)
            yaml.dump(data, cfg)
        config = textwrap.indent(cfg.getvalue(), "  ")
        _log.debug(f"Written the following cloud init user-data:\n{config}")


class Drifter(object):
    def download_file(cls, url: str, target_dir: str) -> str:
        local_filename = os.path.join(
            target_dir,
            url.split('/')[-1]
        )
        with requests.get(url, stream=True) as r:
            r.raise_for_status()
            with open(local_filename, "wb") as f:
                for chunk in r.iter_content(chunk_size=8192):
                    f.write(chunk)
        return local_filename

    def _validate_sig(cls, checksum, sig) -> bool:
        msg = "You need to download and validate the variant manually"
        raise NotImplementedError(msg)

    def _validate_checksum(cls, file, checksum) -> bool:
        msg = "You need to download and validate the variant manually"
        raise NotImplementedError(msg)

    def download_variant(cls, variant: str) -> None:
        target_file = os.path.join(
            Defaults.cache_dir,
            "images",
            f'{variant}.qcow2'
        )
        if os.path.exists(target_file):
            return

        variant_cfg = VARIANT_SOURCE[Variant.from_str(variant)]

        checksum = os.path.join(
            variant_cfg["base_url"],
            variant_cfg["signature"]
        )
        sig = os.path.join(
            variant_cfg["base_url"],
            variant_cfg["checksum"]
        )
        image = os.path.join(
            variant_cfg["base_url"],
            variant_cfg["image"]
        )
        image = cls.download_file(image, Defaults.cache_dir)

        with tempfile.TemporaryDirectory(prefix="drifter_") as tempd:
            checksum = cls.download_file(checksum, tempd)
            sig = cls.download_file(sig, tempd)

        cls._validate_sig(checksum, sig)
        cls._validate_checksum(image, checksum)

    @classmethod
    def run(cls, args: argparse.Namespace) -> None:
        cfg = AnsibleConfig.from_file(args.inventory)

        for m in cfg.machines():
            m.cloud_init()
            m.start()

    @classmethod
    def provision(cls, args: argparse.Namespace) -> None:
        pass

    @classmethod
    def halt(cls, args: argparse.Namespace) -> None:
        cfg = AnsibleConfig.from_file(args.inventory)

        for m in cfg.machines():
            m.halt()

    @classmethod
    def destroy(cls, args: argparse.Namespace) -> None:
        cfg = AnsibleConfig.from_file(args.inventory)
        machines = args.machine
        if len(machines) == 0:
            machines = [i.uid() for i in cfg.machines()]

        for m in cfg.machines():
            if m.uid() in machines:
                m.destroy()

    @classmethod
    def attach(cls, args: argparse.Namespace) -> None:
        cfg = AnsibleConfig.from_file(args.inventory)

        for m in cfg.machines():
            if m.uid() == args.machine:
                m.attach()


def parser() -> argparse.Namespace:
    p = argparse.ArgumentParser()
    p.add_argument(
        "-i", "--inventory",
        default="inventory",
        help="Ansible inventory file (defaults to 'inventory')"
    )
    p.add_argument(
        "-d", "--debug",
        action="store_true",
        help="Enable debug logging"
    )
    subparsers = p.add_subparsers(required=True, dest="mode")

    p_run = subparsers.add_parser("up")
    p_run.set_defaults(func=Drifter.run)

    p_provision = subparsers.add_parser("provision")
    p_provision.set_defaults(func=Drifter.provision)

    p_halt = subparsers.add_parser("halt")
    p_halt.set_defaults(func=Drifter.halt)

    p_destroy = subparsers.add_parser("destroy")
    p_destroy.set_defaults(func=Drifter.destroy)
    p_destroy.add_argument(
        'machine',
        type=str,
        metavar='MACHINE',
        nargs='*',
        help='VM(s) to destroy. Leave empty for all (default).'
    )

    p_attach = subparsers.add_parser("attach")
    p_attach.set_defaults(func=Drifter.attach)
    p_attach.add_argument(
        'machine',
        type=str,
        metavar='MACHINE',
        help="Attaching to a VM's console."
    )

    args = p.parse_args()
    return args


def setup_logging(args: argparse.Namespace) -> None:
    if args.debug:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(
        level=level,
        stream=sys.stderr,
    )


def main() -> int:
    try:
        args = parser()
        setup_logging(args)
        args.func(args)
    except Exception:
        # getting args via locals() in case the argparser fails
        dargs = vars(locals().get("args", argparse.Namespace(debug=True)))
        debug = dargs.get("debug", True)
        if debug:
            traceback.print_exc()
        else:
            print(f"FAILURE: {sys.exc_info()[1]}", file=sys.stderr)
        return 1
    return 0


if __name__ == '__main__':
    sys.exit(main())
