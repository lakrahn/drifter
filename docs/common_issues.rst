Common Issues
=============


QEMU configuration
------------------

When failing with

.. code-block:: console

    stderr=failed to parse default acl file `/etc/qemu/bridge.conf'

you ought to make sure, that the configuration file /etc/qemu/bridge.conf
exists and that it holds the following content:

.. code-block::

    allow virbr0


Missing bridge device
---------------------

When failing with

.. code-block:: console

    stderr=failed to get mtu of bridge `virbr0': No such device

then you have to make sure that the libvirt daemon is running and that the
bridge device 'virbr0' exists.
