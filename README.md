Drifter
=======

This is a first quick shot of a quick vm management tool with focus on Ansible
integration.

Usage
-----

In order to provision your setup you need to provide an ansible inventory like
the following::

	$ drifter.py run -i dev-inventory

This will start the respective machines defined within the inventory and run
the ansible playbook afterwards.


Configuration
-------------

The configuration is fully managed using the ansible inventory. The following
stanzas are being used:

* `ansible_host` To override the ip being used. By default the hostname will
  be resolved and used to define the machine.

* `drifter_cpus` Defines the number of cpus are provided to the vm - defaults
  to 1.

* `drifter_disk_sda` Defines the disk size for the primary disk - defaults to
  10 GiB.

* `drifter_memory` Defines the amount of memory available to the vm -
  defaults to 1GiB.

* `drifter_user_data` Path to a cloud-init
	[user-data](https://cloudinit.readthedocs.io/en/latest/topics/format.html)
	template used for provisioning the vm.

* `drifter_variant` […]
